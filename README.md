# Technical Assignment

This is a Technical Assignment to implement an easy web application that uses the Foursquare API to retreive Venues near your location.

This project was generated using the following technologies:
1. [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0. 
1. [Angular Material](https://material.angular.io) version 9.1.0. 

## Features implemented

1. Location Service.
1. Venue Recommendation Service
1. Pagination

## Features to be implemented in the future

1. Manual Location Service.
1. Venue Information Retrieval Service.

## Dependecies

In orther to install the dependecies of this project you will need to previously have:

1. `node` version 12.5.2
1. `npm` version 6.9.0

Run `npm install` to install the corresponding dependencies for this project.

## Configure VenuesService - Foursquare API

In order to demo this application, you will need to provide the `VenuesService` with the `CLIENT_ID` and the `CLIENT_SECRET` from a [Foursquare App](https://foursquare.com/developers/apps) to use the Foursquare API.

DO NOT SHARE your `CLIENT_ID` and `CLIENT_SECRET` whit anybody, as they are only for your personal use in order to demo this Technical Assignment.

## Development server

Run `node_modules/@angular/cli/bin/ng serve -o` for a dev server using the Angular version this project was desing on. That will let you use the application at `http://localhost:4200/`.

## Running unit tests

Run `node_modules/@angular/cli/bin/ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `node_modules/@angular/cli/bin/ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
