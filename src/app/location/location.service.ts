import { Location } from './location.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private locationBehaviorSubject = new BehaviorSubject(null);
  private locationObservavble: Observable<Location> = this.locationBehaviorSubject.asObservable();
  errorMessage: string;

  constructor() { }

  private setLocation(longitude: number, latitude: number) {
    this.locationBehaviorSubject.next(new Location(longitude, latitude));
  }

  getLocationObservable(): Observable<Location> {
    return this.locationObservavble;
  }

  requestLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          position => {
            this.setLocation(position.coords.longitude, position.coords.latitude);
          },
          () => {
            this.errorMessage = 'Sorry! There was a problem determining your location. Try again later.';
          }
        );
    } else {
      this.errorMessage = 'Sorry! Your browser does not support this functionality.';
    }
  }

}
