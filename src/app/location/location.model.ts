export interface ILocation {
    longitude?: number;
    latitude?: number;
}

export class Location implements ILocation {
  constructor(
      public longitude?: number,
      public latitude?: number
    ) { }
}
