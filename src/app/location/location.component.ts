import { Location } from './location.model';
import { LocationService } from './location.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  locationFetched = false;

  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
    this.getLocationObservable().subscribe(location => {
        this.locationFetched = !!location;
    });
  }

  requestLocation() {
    this.locationService.requestLocation();
  }

  getLocationObservable(): Observable<Location> {
    return this.locationService.getLocationObservable();
  }

  getLocationErrorMessage(): string {
    return this.locationService.errorMessage;
  }

}
