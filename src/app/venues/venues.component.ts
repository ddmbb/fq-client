import { VenuesService } from './venues.service';
import { Location } from './../location/location.model';
import { LocationService } from './../location/location.service';
import { Component, OnInit } from '@angular/core';
import { Venue } from './venue.model';

@Component({
  selector: 'app-venues',
  templateUrl: './venues.component.html',
  styleUrls: ['./venues.component.scss']
})
export class VenuesComponent implements OnInit {
  venues: Venue[];
  offset = 0;
  geo: Location;

  constructor(
    private locationService: LocationService,
    private venuesService: VenuesService
  ) { }

  ngOnInit(): void {
    this.locationService.getLocationObservable().subscribe(location => {
      if (location) {
        this.geo = location;
        this.getVenuesNearLocation(location);
      }
    });
  }

  getVenuesNearLocation(location: Location, offset: number = 0) {
    this.venuesService.getVenuesNearLocation(location, offset).subscribe(
      reply => {
        this.venues = reply.body.response.groups[0]?.items?.map(item =>
          ({
            id: item.venue.id,
            category: item.venue.categories[0].name,
            iconUrl: item.venue.categories[0].icon.prefix + 44 + item.venue.categories[0].icon.suffix,
            name: item.venue.name,
            formattedAddress: item.venue.location.formattedAddress
        }) as Venue);
      },
      error => console.log(error)
    );
  }

  getNextVenues() {
    if (this.geo) {
      this.offset++;
      this.getVenuesNearLocation(this.geo, this.offset);
    }
  }

  getPreviousVenues() {
    if (this.geo && this.offset > 0) {
      this.offset--;
      this.getVenuesNearLocation(this.geo, this.offset);
    }
  }

}
