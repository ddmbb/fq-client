export interface IVenue {
    id?: string;
    category?: string;
    name?: string;
    formattedAddress?: string[];
    iconUrl?: string;
}

export class Venue implements IVenue {
    constructor(
        public id?: string,
        public category?: string,
        public name?: string,
        public formattedAddress?: string[],
        public iconUrl?: string
    ) {}
}
