import { Location } from './../location/location.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VenuesService {
  private CLIENT_ID = '';
  private CLIENT_SECRET = '';

  private BASE_FOURSQUARE_URL = 'https://api.foursquare.com/v2/venues';
  private CLIENT_URL_QUERY = `client_id=${this.CLIENT_ID}&client_secret=${this.CLIENT_SECRET}`;

  constructor(private http: HttpClient) { }

  getVenuesNearLocation(location: Location, offset: number = 0): Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.BASE_FOURSQUARE_URL}/explore?ll=${location.latitude}` +
    `,${location.longitude}&${this.CLIENT_URL_QUERY}&offset=${offset}&v=20200331`,
    { observe: 'response' }
    );
  }
}
